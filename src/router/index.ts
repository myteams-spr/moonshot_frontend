import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import User from '@/pages/users/Users.vue';
import Roles from '@/pages/roles/Roles.vue';
import CreateRole from '@/pages/roles/CreateRole.vue';
import EditRole from '@/pages/roles/EditRole.vue';

import CreateUser from '@/pages/users/CreateUser.vue';
import Wrapper from '@/pages/Wrapper.vue';

import EditUser from '@/pages/users/EditUser.vue';
import Dashboard from '@/pages/Dashboard.vue';

import Register from '@/pages/Register.vue';
import Login from '@/pages/Login.vue';
import Layout from '@/pages/Layout.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/register',
    component: Register,
  },
  {
    path: '/login',
    component: Login,
  },

  /**
   * Uncomment this child routes after completion of all procedures(after backend completion)
   * after login and register completed
   */
  // {
  //   path: '',
  //   component: Wrapper,
  //   children: [
  { path: '', component: Dashboard },
  { path: '/users', component: User },

  { path: '/users/create', component: CreateUser },
  { path: '/users/:id/edit', component: EditUser },

  { path: '/roles', component: Roles },
  { path: '/roles/create', component: CreateRole },
  { path: '/roles/:id/edit', component: EditRole },

  { path: '/layout', component: Layout },
  // ],
  // },

  /**
   * This is how the child route works
   * Dashboard component and user component lies inside the navbar layout
   * The register component lies outside of the wrapper layout
   */

  // {
  //   path: '/login',
  //   component: Login,
  // },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
