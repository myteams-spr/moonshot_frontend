import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import '../src/tailwind/index.css';

import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/';

axios.defaults.withCredentials = false; //true upon login header defined
/**
 * for login can be seen on th application as local storage
 *this is needed globally declaration
 * because when a route needed specific authenticated user
 * Most of the routes needs authenticated user so it is good option to place globally
 * */

createApp(App).use(store).use(router).mount('#app');
