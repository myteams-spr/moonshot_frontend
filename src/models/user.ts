import { Role } from './role';

export class User {
  constructor(
    public id: number,
    public first_name: string,
    public last_name: string,
    public email: string,
    public role: Role
    /**
     * for class initializing property using keyword public or private
     * passing inside constructor we don't have to initialize as this.id = 0 and bla bla bla ...
     *
     */
  ) {}
}
